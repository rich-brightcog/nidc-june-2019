package nidc.functions.`as`.values

import arrow.syntax.function.partially1
import arrow.syntax.function.curried

fun main() {
    fun add(no1: Int, no2: Int)  = no1 + no2
    val subtract = { no1: Int, no2: Int -> no1 - no2 }

    println(add(34,12))
    println(subtract(34,12))

    val f1 = subtract.partially1(34)
    println(f1(12))

    val f2 = subtract.curried()
    println(f2(34)(12))
}