package nidc.functions.composing

import arrow.syntax.function.*

val square = { x:Int -> x * x }
val half = { x:Int -> x / 2 }
val print = { x:Int -> println(x) }

fun main() {
    val f1 =  print compose half compose square
    val f2 =  square andThen half andThen print
    f1(8)
    f2(8)
}