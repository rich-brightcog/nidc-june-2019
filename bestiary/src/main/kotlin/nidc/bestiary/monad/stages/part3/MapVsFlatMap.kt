package nidc.bestiary.monad.stages.part3

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import arrow.core.getOrElse

val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    System.setProperty("foo", "bar")
    System.setProperty("bar", "zed")
    System.setProperty("zed", "Eureka!")

    val result1: Option<String> = property("foo")
    val result2: Option<String> = result1.flatMap { property(it) }
    val result3: Option<String> = result2.flatMap { property(it) }
    println(result3.getOrElse { "Foiled Again!" })

    println(
        property("foo")
            .flatMap { property(it) }
            .flatMap { property(it) }
            .getOrElse { "Foiled Again!" })
}