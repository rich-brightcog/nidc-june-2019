package nidc.bestiary.monad.stages.part2

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import arrow.core.Some

val property = { name: String -> fromNullable(System.getProperty(name)) }

val propertyOpt = { optName: Option<String> ->
    if(optName is Some<String>) {
        fromNullable(System.getProperty(optName.t))
    } else {
        Option.empty()
    }
}

fun main() {
    System.setProperty("foo", "bar")
    System.setProperty("bar", "zed")
    System.setProperty("zed", "Eureka!")

    val result1: Option<String> = property("foo")
    val result2: Option<Option<String>> = result1.map { property(it) }
    val result3: Option<Option<String>> = result2.map { propertyOpt(it) }

    if(result3 is Some<Option<String>>) {
        val innerOpt = result3.t
        if(innerOpt is Some<String>) {
            println(innerOpt.t)
        }
    }
}