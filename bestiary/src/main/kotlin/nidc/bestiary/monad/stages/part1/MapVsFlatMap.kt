package nidc.bestiary.monad.stages.part1

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable

val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    System.setProperty("foo", "bar")
    System.setProperty("bar", "zed")
    System.setProperty("zed", "Eureka!")

    val result1: Option<String> = property("foo")
    val result2: Option<Option<String>> = result1.map { property(it) }

    //TODO: What do we do next?
    println(result2)
}