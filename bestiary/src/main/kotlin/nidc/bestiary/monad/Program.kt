package nidc.bestiary.monad

import arrow.core.Option.Companion.fromNullable
import arrow.core.extensions.option.monad.binding
import arrow.core.getOrElse

val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    System.setProperty("foo","bar")
    System.setProperty("bar","zed")
    System.setProperty("zed","Eureka!")

    val result = binding {
        val first = property("foo").bind()
        val second = property(first).bind()
        property(second).bind()
    }
    println(result.getOrElse { "Foiled Again!" })

}