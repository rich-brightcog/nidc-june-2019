package nidc.bestiary.bifunctor

import arrow.core.Left
import arrow.core.Option
import arrow.core.Right

val property = { name: String -> Option.fromNullable(System.getProperty(name)) }
val propertyOrElse = { name: String, default: String ->
    property(name).fold({ Left(default) }, { Right(it) })
}
val printHappy = { str: String -> println("$str (စ‿စ)") }
val printSad = { str: String -> println("$str ¯\\_(ツ)_/¯") }

fun main() {
    val either1 = propertyOrElse("java.vendor", "Invalid")
    val either2 = propertyOrElse("java.wibble", "Invalid")

    either1.bimap(printSad, printHappy)
    either2.bimap(printSad, printHappy)
}