package nidc.bestiary.applicative

import arrow.core.Option.Companion.fromNullable
import arrow.core.getOrElse
import arrow.syntax.function.curried

val concat = { s1: String, s2: String  -> "$s1 $s2" }
val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    val opt1 = property("java.vendor")
    val opt2 = property("java.version")

    val concatCurry = concat.curried()
    val result = opt1.ap(opt2.map(concatCurry))

    println(result.getOrElse { "Whoops" })
}