package com.instil.rebelcon.server

class DeletionException(message: String) : RuntimeException(message)