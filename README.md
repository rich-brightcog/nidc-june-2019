# The Bestiary of Pure FP

This repo contains the materials for the [Bestiary of Pure FP talk](https://www.nidevconf.com/sessions/garthgilmourrichardgibson/) at NIDC 2019.

The folders are as follows:

1. Slides. The slide deck used during the talk
2. Bestiary. A project containing examples of FP Patterns (such as Functor and Applicative)
3. Effects-server. A RESTful service written in Spring Boot
4. Effects-client. Console clients for the above. One written via procedural code and the other using Arrow Fx.


