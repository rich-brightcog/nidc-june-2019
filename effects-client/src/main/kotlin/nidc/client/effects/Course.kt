package nidc.client.effects

enum class CourseDifficulty {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}

class Course(
    var id: String,
    var title: String,
    var difficulty: CourseDifficulty,
    var duration: Int
) {
    constructor() : this("", "", CourseDifficulty.BEGINNER, 0)

    override fun toString(): String {
        return "[$id] $title - a $duration day ${difficulty.toString().toLowerCase()} course"
    }
}