package nidc.client.effects

import arrow.core.andThen
import arrow.data.ListK
import arrow.data.k
import org.glassfish.jersey.media.sse.EventInput
import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException
import javax.ws.rs.client.Invocation.Builder
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import javax.ws.rs.sse.InboundSseEvent
import kotlin.system.exitProcess


import arrow.effects.*
import arrow.effects.IO.Companion.raiseError
import arrow.effects.extensions.io.applicative.applicative
import arrow.effects.extensions.io.fx.fx
import arrow.effects.typeclasses.ExitCase
import javax.ws.rs.client.Client

object Program {
    private val client = ClientBuilder.newClient()

    private const val baseURL = "http://localhost:8080/courses/"

    private fun putStrLn(s: String): IO<Unit> = IO { println(s) }

    private fun getStrLn(): IO<String> = IO { readLine() ?: "" }

    private fun stringToInt(input: String): IO<Int> =
        IO { input.trim().toInt() }.handleErrorWith { raiseError(it) }


    private fun printMenu(): IO<Unit> = putStrLn(
        """
        --------------------------------
        1) View all the current courses
        2) View a course by its number
        3) Remove a course by number
        4) Update a courses details
        5) Exit
    """.trimIndent()
    )



    private val clientRelease: (Client, ExitCase<Throwable>) -> IO<Unit> = { client, exitCase ->
        when (exitCase) {
            is ExitCase.Error -> IO { client.close() }
            else -> IO { Unit }
        }
    }

    private fun <T> Client.safeOp(f: (Client) -> T): IO<T> =
        IO { this }.bracketCase(
            release = clientRelease,
            use = f andThen { IO.just(it) })



    private fun eventSequence(event: EventInput): ListK<InboundSseEvent> =
        generateSequence {
            if (!event.isClosed) event.read() else null
        }.toList().k()

    inline fun <reified T> unMarshall(event: InboundSseEvent): IO<T> = IO {
        event.readData(T::class.java, APPLICATION_JSON_TYPE)
    }


    private fun viewAllCourses(): IO<Unit> = fx {
        val eventInput = !IO { client.target(baseURL).request().get(EventInput::class.java) }
        val courses = !eventSequence(eventInput).traverse(IO.applicative()) {
            unMarshall<Course>(
                it
            )
        }
        !effect { courses.forEach(::println) }
    }


    private fun viewCourseById(): IO<Unit> = fx {
        !askForCourseId()
        val id = !getStrLn()
        val course = !loadCourseById(id)
        !putStrLn(course.toString())
    }

    private fun loadCourseById(id: String): IO<Course> = client.safeOp {
        it.target(baseURL)
            .path(id)
            .request(MediaType.APPLICATION_JSON)
            .get(Course::class.java)
    }

    private fun deleteCourseById(id: String): IO<String> = client.safeOp {
        it.target(baseURL)
            .path(id)
            .request()
            .delete(String::class.java)
    }


    private fun removeCourseById(): IO<Unit>
            = fx {
        !askForCourseId()
        val id = !getStrLn()
        val message = !deleteCourseById(id)
        !putStrLn(message)
    }

    private fun updateCourse(): IO<Unit> {
        fun <T> readField(name: String, default: T): IO<String> = fx {
            val defaultStr = default.toString()
            !putStrLn("Enter the new $name (default '$defaultStr')")
            val line = !getStrLn()
            !effect { if (line.isNotEmpty()) line else defaultStr }
        }

        fun readNewDetails(course: Course): IO<Course> = fx {
            val title = !readField("title", course.title)
            val durationStr = !readField("duration", course.duration)
            val duration = !stringToInt(durationStr)
            val field = !readField("difficulty", course.difficulty)
            val difficulty = !effect { CourseDifficulty.valueOf(field) }
            !effect { Course(course.id, title, difficulty, duration) }
        }

        fun sendUpdate(input: Course) = IO {
            client.target(baseURL)
                .path(input.id)
                .request()
                .put(Entity.json(input))
        }

        return fx {
            !putStrLn("Enter the id of the course to update")
            val id = !getStrLn()
            val oldCourse = !loadCourseById(id)
            val newCourse = !readNewDetails(oldCourse)
            !putStrLn("persisting new course $newCourse")
            !sendUpdate(newCourse)
            !putStrLn("Update sent")

        }
    }


    private fun exit(): IO<Unit> = IO {
        client.close()
        exitProcess(0)
    }

    private fun askForCourseId() = putStrLn("Enter the course id")
    private fun failOnNoSuchCourse() = putStrLn("No such course exists")
    private fun failOnCannotBeRemoved() = putStrLn("This course cannot be removed")
    private fun failOnInvalidChoice() = putStrLn("Number not valid")
    private fun failOnUnhandledError(t: Throwable) = fx {
        !effect { t.printStackTrace() }
        !putStrLn("Unhandled error: ${t.message ?: ""}")
    }

    private fun failOnCannotReadData() = putStrLn("New value cannot be understood")


    private fun commandLoop(): IO<Unit> = fx {
        !printMenu()
        val choice = !getStrLn()
        val choiceInt = !stringToInt(choice)
        !mapToHandler(choiceInt)
    }.handleErrorWith {
        when (it) {
            is NotFoundException -> failOnNoSuchCourse()
            is BadRequestException -> failOnCannotBeRemoved()
            is NumberFormatException,
            is IllegalArgumentException -> failOnCannotReadData()
            else -> failOnUnhandledError(it)
        }.map(::println)
    }

    private fun mapToHandler(choice: Int): IO<Unit> = fx {
        when (choice) {
            1 -> {
                !viewAllCourses()
                !commandLoop()
            }
            2 -> {
                !viewCourseById()
                !commandLoop()
            }
            3 -> {
                !removeCourseById()
                !commandLoop()
            }
            4 -> {
                !updateCourse()
                !commandLoop()
            }
            5 -> !exit()
            else -> !failOnInvalidChoice()
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        commandLoop().unsafeRunSync()
    }
}

