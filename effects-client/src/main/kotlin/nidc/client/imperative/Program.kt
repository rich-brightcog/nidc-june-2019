package nidc.client.imperative

import org.glassfish.jersey.media.sse.EventInput
import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import javax.ws.rs.sse.InboundSseEvent
import kotlin.system.exitProcess

object Program {
    private val client = ClientBuilder.newClient()
    private const val baseURL = "http://localhost:8080/courses/"

    @JvmStatic
    fun main(args: Array<String>) {
        while (true) {
            printMenu()
            val choice = readLine()?.toIntOrNull()
            val handler = mapToHandler(choice)
            handler()
        }
    }

    private fun printMenu() {
        val menu = """
        --------------------------------
        1) View all the current courses
        2) View a course by its number
        3) Remove a course by number
        4) Update a courses details
        5) Exit
    """.trimIndent()
        println(menu)
    }

    private fun mapToHandler(choice: Int?) = when (choice) {
        1 -> Program::viewAllCourses
        2 -> Program::viewCourseById
        3 -> Program::removeCourseById
        4 -> Program::updateCourse
        5 -> Program::exit
        is Int -> Program::failOnInvalidChoice
        else -> Program::failOnNotRecognised
    }

    private fun viewAllCourses() {
        fun unMarshall(event: InboundSseEvent): Course {
            return event.readData(Course::class.java, APPLICATION_JSON_TYPE)
        }

        val input = client
            .target(baseURL)
            .request()
            .get(EventInput::class.java)
        while (!input.isClosed) {
            val event = input.read()
            if (event != null) {
                println(unMarshall(event))
            }
        }
    }

    private fun viewCourseById() {
        askForCourseId()
        val id = readLine()
        if (id != null) {
            try {
                println(loadCourseById(id))
            } catch(_: NotFoundException) {
                failOnNoSuchCourse()
            }
        }
    }

    private fun loadCourseById(id: String): Course {
        return client
            .target(baseURL)
            .path(id)
            .request(MediaType.APPLICATION_JSON)
            .get(Course::class.java)
    }

    private fun removeCourseById() {
        askForCourseId()
        val id = readLine()
        if (id != null) {
            try {
                val message = client
                    .target(baseURL)
                    .path(id)
                    .request()
                    .delete(String::class.java)
                println(message)
            } catch(_: NotFoundException) {
                failOnNoSuchCourse()
            } catch(_: BadRequestException) {
                failOnCannotBeRemoved()
            }
        }
    }

    private fun updateCourse() {
        fun <T> readField(name: String, default: T): String {
            val defaultStr = default.toString()
            println("Enter the new $name (default '$defaultStr')")
            val line = readLine()
            return if (line != null && line.isNotEmpty()) line else defaultStr
        }

        fun readNewDetails(course: Course): Course {
            val title = readField("title", course.title)
            val duration = readField("duration", course.duration).toInt()
            val difficulty = CourseDifficulty.valueOf(readField("difficulty", course.difficulty))
            return Course(course.id, title, difficulty, duration)
        }

        fun sendUpdate(input: Course) = client
                .target(baseURL)
                .path(input.id)
                .request()
                .put(Entity.json(input),String::class.java)

        println("Enter the id of the course to update")
        val id = readLine()
        if (id != null) {
            try {
                val oldCourse = loadCourseById(id)
                val newCourse = readNewDetails(oldCourse)
                sendUpdate(newCourse)
            } catch (ex : Exception) {
                when(ex) {
                    is NotFoundException -> failOnNoSuchCourse()
                    is NumberFormatException,
                    is IllegalArgumentException -> failOnCannotReadData()
                }
            }
        }
    }

    private fun exit() {
        client.close()
        exitProcess(0)
    }

    private fun askForCourseId() = println("Enter the course id")
    private fun failOnNoSuchCourse() = println("No such course exists")
    private fun failOnCannotBeRemoved() = println("This course cannot be removed")
    private fun failOnInvalidChoice() = println("Number not valid")
    private fun failOnNotRecognised() = println("Unknown input")
    private fun failOnCannotReadData() = println("New value cannot be understood")
}
